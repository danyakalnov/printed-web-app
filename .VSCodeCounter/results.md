# Summary

Date : 2020-01-30 18:04:08

Directory c:\WebDevelopment\printed\src

Total : 47 files,  2201 codes, 46 comments, 354 blanks, all 2601 lines

[details](details.md)

## Languages
| language | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| JavaScript | 34 | 1,982 | 43 | 326 | 2,351 |
| CSS | 13 | 219 | 3 | 28 | 250 |

## Directories
| path | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| . | 47 | 2,201 | 46 | 354 | 2,601 |
| components | 20 | 761 | 8 | 121 | 890 |
| components\Orders | 4 | 282 | 3 | 45 | 330 |
| components\Orders\OrdersPool | 4 | 282 | 3 | 45 | 330 |
| components\Orders\OrdersPool\Order | 2 | 161 | 0 | 26 | 187 |
| components\Profile | 2 | 304 | 3 | 45 | 352 |
| components\UI | 14 | 175 | 2 | 31 | 208 |
| components\UI\Backdrop | 2 | 15 | 0 | 3 | 18 |
| components\UI\Card | 2 | 12 | 0 | 4 | 16 |
| components\UI\DocumentIcon | 1 | 11 | 0 | 2 | 13 |
| components\UI\InputError | 1 | 13 | 0 | 2 | 15 |
| components\UI\Modal | 2 | 41 | 0 | 6 | 47 |
| components\UI\NavigationItems | 4 | 61 | 2 | 9 | 72 |
| components\UI\NavigationItems\NavigationItem | 2 | 40 | 2 | 7 | 49 |
| components\UI\SideBar | 2 | 22 | 0 | 5 | 27 |
| containers | 5 | 366 | 0 | 52 | 418 |
| containers\Auth | 5 | 366 | 0 | 52 | 418 |
| containers\Auth\Logout | 1 | 18 | 0 | 5 | 23 |
| containers\Auth\SpotRegister | 2 | 118 | 0 | 16 | 134 |
| hoc | 3 | 48 | 0 | 10 | 58 |
| hoc\Adapt | 1 | 7 | 0 | 2 | 9 |
| hoc\Auxiliary | 1 | 2 | 0 | 1 | 3 |
| hoc\withErrorHandler | 1 | 39 | 0 | 7 | 46 |
| store | 11 | 781 | 0 | 124 | 905 |
| store\actions | 6 | 495 | 0 | 83 | 578 |
| store\reducers | 4 | 274 | 0 | 40 | 314 |

[details](details.md)