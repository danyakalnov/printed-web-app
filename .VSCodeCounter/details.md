# Details

Date : 2020-01-30 18:04:08

Directory c:\WebDevelopment\printed\src

Total : 47 files,  2201 codes, 46 comments, 354 blanks, all 2601 lines

[summary](results.md)

## Files
| filename | language | code | comment | blank | total |
| :--- | :--- | ---: | ---: | ---: | ---: |
| [App.css](file:///c%3A/WebDevelopment/printed/src/App.css) | CSS | 33 | 0 | 6 | 39 |
| [App.js](file:///c%3A/WebDevelopment/printed/src/App.js) | JavaScript | 63 | 0 | 12 | 75 |
| [App.test.js](file:///c%3A/WebDevelopment/printed/src/App.test.js) | JavaScript | 8 | 0 | 2 | 10 |
| [axios-printed.js](file:///c%3A/WebDevelopment/printed/src/axios-printed.js) | JavaScript | 5 | 0 | 2 | 7 |
| [components\Orders\OrdersPool\Order\Order.js](file:///c%3A/WebDevelopment/printed/src/components/Orders/OrdersPool/Order/Order.js) | JavaScript | 151 | 0 | 26 | 177 |
| [components\Orders\OrdersPool\Order\Order.module.css](file:///c%3A/WebDevelopment/printed/src/components/Orders/OrdersPool/Order/Order.module.css) | CSS | 10 | 0 | 0 | 10 |
| [components\Orders\OrdersPool\OrdersPool.js](file:///c%3A/WebDevelopment/printed/src/components/Orders/OrdersPool/OrdersPool.js) | JavaScript | 92 | 3 | 16 | 111 |
| [components\Orders\OrdersPool\OrdersPool.module.css](file:///c%3A/WebDevelopment/printed/src/components/Orders/OrdersPool/OrdersPool.module.css) | CSS | 29 | 0 | 3 | 32 |
| [components\Profile\Profile.js](file:///c%3A/WebDevelopment/printed/src/components/Profile/Profile.js) | JavaScript | 274 | 2 | 39 | 315 |
| [components\Profile\Profile.module.css](file:///c%3A/WebDevelopment/printed/src/components/Profile/Profile.module.css) | CSS | 30 | 1 | 6 | 37 |
| [components\UI\Backdrop\Backdrop.js](file:///c%3A/WebDevelopment/printed/src/components/UI/Backdrop/Backdrop.js) | JavaScript | 6 | 0 | 3 | 9 |
| [components\UI\Backdrop\Backdrop.module.css](file:///c%3A/WebDevelopment/printed/src/components/UI/Backdrop/Backdrop.module.css) | CSS | 9 | 0 | 0 | 9 |
| [components\UI\Card\Card.js](file:///c%3A/WebDevelopment/printed/src/components/UI/Card/Card.js) | JavaScript | 6 | 0 | 4 | 10 |
| [components\UI\Card\Card.module.css](file:///c%3A/WebDevelopment/printed/src/components/UI/Card/Card.module.css) | CSS | 6 | 0 | 0 | 6 |
| [components\UI\DocumentIcon\DocumentIcon.js](file:///c%3A/WebDevelopment/printed/src/components/UI/DocumentIcon/DocumentIcon.js) | JavaScript | 11 | 0 | 2 | 13 |
| [components\UI\InputError\InputError.js](file:///c%3A/WebDevelopment/printed/src/components/UI/InputError/InputError.js) | JavaScript | 13 | 0 | 2 | 15 |
| [components\UI\Modal\Modal.js](file:///c%3A/WebDevelopment/printed/src/components/UI/Modal/Modal.js) | JavaScript | 28 | 0 | 6 | 34 |
| [components\UI\Modal\Modal.module.css](file:///c%3A/WebDevelopment/printed/src/components/UI/Modal/Modal.module.css) | CSS | 13 | 0 | 0 | 13 |
| [components\UI\NavigationItems\NavigationItem\NavigationItem.js](file:///c%3A/WebDevelopment/printed/src/components/UI/NavigationItems/NavigationItem/NavigationItem.js) | JavaScript | 14 | 0 | 2 | 16 |
| [components\UI\NavigationItems\NavigationItem\NavigationItem.module.css](file:///c%3A/WebDevelopment/printed/src/components/UI/NavigationItems/NavigationItem/NavigationItem.module.css) | CSS | 26 | 2 | 5 | 33 |
| [components\UI\NavigationItems\NavigationItems.js](file:///c%3A/WebDevelopment/printed/src/components/UI/NavigationItems/NavigationItems.js) | JavaScript | 12 | 0 | 2 | 14 |
| [components\UI\NavigationItems\NavigationItems.module.css](file:///c%3A/WebDevelopment/printed/src/components/UI/NavigationItems/NavigationItems.module.css) | CSS | 9 | 0 | 0 | 9 |
| [components\UI\SideBar\SideBar.js](file:///c%3A/WebDevelopment/printed/src/components/UI/SideBar/SideBar.js) | JavaScript | 11 | 0 | 3 | 14 |
| [components\UI\SideBar\SideBar.module.css](file:///c%3A/WebDevelopment/printed/src/components/UI/SideBar/SideBar.module.css) | CSS | 11 | 0 | 2 | 13 |
| [containers\Auth\Auth.js](file:///c%3A/WebDevelopment/printed/src/containers/Auth/Auth.js) | JavaScript | 213 | 0 | 29 | 242 |
| [containers\Auth\Auth.module.css](file:///c%3A/WebDevelopment/printed/src/containers/Auth/Auth.module.css) | CSS | 17 | 0 | 2 | 19 |
| [containers\Auth\Logout\Logout.js](file:///c%3A/WebDevelopment/printed/src/containers/Auth/Logout/Logout.js) | JavaScript | 18 | 0 | 5 | 23 |
| [containers\Auth\SpotRegister\SpotRegister.js](file:///c%3A/WebDevelopment/printed/src/containers/Auth/SpotRegister/SpotRegister.js) | JavaScript | 101 | 0 | 14 | 115 |
| [containers\Auth\SpotRegister\SpotRegister.module.css](file:///c%3A/WebDevelopment/printed/src/containers/Auth/SpotRegister/SpotRegister.module.css) | CSS | 17 | 0 | 2 | 19 |
| [hoc\Adapt\Adapt.js](file:///c%3A/WebDevelopment/printed/src/hoc/Adapt/Adapt.js) | JavaScript | 7 | 0 | 2 | 9 |
| [hoc\Auxiliary\Auxiliary.js](file:///c%3A/WebDevelopment/printed/src/hoc/Auxiliary/Auxiliary.js) | JavaScript | 2 | 0 | 1 | 3 |
| [hoc\withErrorHandler\withErrorHandler.js](file:///c%3A/WebDevelopment/printed/src/hoc/withErrorHandler/withErrorHandler.js) | JavaScript | 39 | 0 | 7 | 46 |
| [index.css](file:///c%3A/WebDevelopment/printed/src/index.css) | CSS | 9 | 0 | 2 | 11 |
| [index.js](file:///c%3A/WebDevelopment/printed/src/index.js) | JavaScript | 32 | 3 | 9 | 44 |
| [serviceWorker.js](file:///c%3A/WebDevelopment/printed/src/serviceWorker.js) | JavaScript | 94 | 31 | 13 | 138 |
| [setupTests.js](file:///c%3A/WebDevelopment/printed/src/setupTests.js) | JavaScript | 1 | 4 | 1 | 6 |
| [store\actions\account.js](file:///c%3A/WebDevelopment/printed/src/store/actions/account.js) | JavaScript | 33 | 0 | 5 | 38 |
| [store\actions\actionTypes.js](file:///c%3A/WebDevelopment/printed/src/store/actions/actionTypes.js) | JavaScript | 27 | 0 | 3 | 30 |
| [store\actions\auth.js](file:///c%3A/WebDevelopment/printed/src/store/actions/auth.js) | JavaScript | 218 | 0 | 37 | 255 |
| [store\actions\index.js](file:///c%3A/WebDevelopment/printed/src/store/actions/index.js) | JavaScript | 22 | 0 | 3 | 25 |
| [store\actions\order.js](file:///c%3A/WebDevelopment/printed/src/store/actions/order.js) | JavaScript | 107 | 0 | 20 | 127 |
| [store\actions\spot.js](file:///c%3A/WebDevelopment/printed/src/store/actions/spot.js) | JavaScript | 88 | 0 | 15 | 103 |
| [store\reducers\account.js](file:///c%3A/WebDevelopment/printed/src/store/reducers/account.js) | JavaScript | 29 | 0 | 5 | 34 |
| [store\reducers\auth.js](file:///c%3A/WebDevelopment/printed/src/store/reducers/auth.js) | JavaScript | 109 | 0 | 13 | 122 |
| [store\reducers\order.js](file:///c%3A/WebDevelopment/printed/src/store/reducers/order.js) | JavaScript | 76 | 0 | 13 | 89 |
| [store\reducers\spot.js](file:///c%3A/WebDevelopment/printed/src/store/reducers/spot.js) | JavaScript | 60 | 0 | 9 | 69 |
| [store\utility.js](file:///c%3A/WebDevelopment/printed/src/store/utility.js) | JavaScript | 12 | 0 | 1 | 13 |

[summary](results.md)