import React, { useEffect } from 'react';
import classes from './SpotRegister.module.css';
import printedLogo from '../../../assets/images/printed-logo.png';

import { Form, Field } from 'react-final-form';
import adapt from '../../../hoc/Adapt/Adapt';
import Error from '../../../components/UI/InputError/InputError';
import { Label, Input, FormGroup, Button } from 'smooth-ui';

import withErrorHandler from '../../../hoc/withErrorHandler/withErrorHandler';
import axios from '../../../axios-printed';
import { connect } from 'react-redux';
import * as actions from '../../../store/actions/index';

const SpotRegister = props => {

  useEffect(() => {
    if (props.address) {
      props.onSetSuccessAuthentication();
    }
  }, [props]);

  const spotCreateHandler = spotData => {
    let spotCreationData = {};
    spotCreationData.address = `${spotData.city}, ул. ${spotData.street}, ${spotData.house}`;
    spotCreationData.location = [0, 0];
    spotCreationData.status = "free";
    spotCreationData.token = props.token;

    console.log(spotCreationData);
    
    props.onCreateSpot(spotCreationData);
  };

  const AdaptedInput = adapt(Input);

  const required = value => {
    return value ? undefined: 'Обязательное поле';
  }

  const spotCreationForm = (
    <Form 
      onSubmit={spotCreateHandler}
      render={({handleSubmit, form, submitting, pristine}) => (
        <form onSubmit={handleSubmit}>
          <FormGroup>
            <Label>Город</Label>
            <Field 
              name="city" 
              validate={required}
              component={AdaptedInput}
              placeholder="Ваш город"
              control
              />
            <Error name="city" />
          </FormGroup>
          <FormGroup>
            <Label>Улица</Label>
            <Field 
              name="street" 
              validate={required}
              component={AdaptedInput}
              placeholder="Ваша улица"
              control
              />
            <Error name="street" />
          </FormGroup>
          <FormGroup>
            <Label>Дом</Label>
            <Field 
              name="house" 
              validate={required}
              component={AdaptedInput}
              placeholder="Номер дома(с буквой, если есть)"
              control
            />
            <Error name="house" />
          </FormGroup>
          <Button 
            style={{backgroundColor: '#52DC22', margin: '15px'}}
            type="submit" 
            disabled={submitting || pristine}
            variant="primary">
            Создать точку печати
          </Button>
        </form>
      )}
    />
  );

  return (
    <div className={classes.SpotRegister}>
      <div className={classes.Logo}>
        <img src={printedLogo} alt="Printed Logo"/>
      </div>
      {spotCreationForm}
    </div>
  );
};

const mapStateToProps = state => {
  return {
    address: state.spot.address,
    token: state.auth.token
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onCreateSpot: spotData => dispatch(actions.createSpot(spotData)),
    onSetSuccessAuthentication: () => dispatch(actions.setSuccessAuthentication())
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(withErrorHandler(SpotRegister, axios));