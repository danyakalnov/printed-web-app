import React, { useState } from 'react';
import { Redirect } from 'react-router-dom';

import { parsePhoneNumberFromString } from 'libphonenumber-js';

import { Form, Field } from 'react-final-form';
import adapt from '../../hoc/Adapt/Adapt';
import Error from '../../components/UI/InputError/InputError';
import { Label, Input, FormGroup, Button } from 'smooth-ui';

import printedLogo from '../../assets/images/printed-logo.png';
import classes from './Auth.module.css';

import withErrorHandler from '../../hoc/withErrorHandler/withErrorHandler';
import axios from '../../axios-printed';

import { connect } from 'react-redux';
import * as actions from '../../store/actions/index';

const Auth = props => {
  const [isSignUp, setSignUp] = useState(false);

  const signUpHandler = (registrationDataRaw) => {
    const registrationData = {
      ...registrationDataRaw
    };

    registrationData.phoneNumber = getFormattedPhoneNumber(registrationData.phoneNumber);
    registrationData.creditCard = registrationData.creditCard.slice(-4);
    registrationData.roleName = 'manager';

    props.onSignUp(registrationData);
  };

  const signInHandler = (loginDataRaw) => {
    const loginData = {
      ...loginDataRaw
    };

    loginData.phoneNumber = getFormattedPhoneNumber(loginData.phoneNumber);
    props.onSignIn(loginData);
  };

  const switchAuthModeHandler = () => {
    setSignUp(prev => !prev);
  }

  const getFormattedPhoneNumber = phoneNumber => {
    const phoneNum = parsePhoneNumberFromString(phoneNumber);
    return phoneNum.number;
  };

  const required = value => {
    return value ? undefined: 'Обязательное поле';
  }

  const validateUsername = name => {
    const pattern = /^([А-Яа-я]){2,20}$/;
    let isValidUsername = pattern.test(name);
    return isValidUsername ? undefined : 'Имя должно содержать только буквы и быть не более 20 символов';
  }

  const validateEmail = email => {
    const pattern = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/;
    let isValidEmail = pattern.test(email);
    return isValidEmail ? undefined : 'Неверный e-mail';
  }

  const validatePhoneNumber = inputPhoneNumber => {
    const phoneNumber = parsePhoneNumberFromString(inputPhoneNumber);
    if (phoneNumber) {
      console.log(phoneNumber.isValid());
      return phoneNumber.isValid() ? undefined : 'Некорректный номер';
    }

    return undefined;
  }

  const validatePassword = inputPassword => {
    const pattern = /^(?=.*[A-Z])(?=.*\d)[A-Za-z\d@$!%*?&]{8,}$/;
    let isValidPassword = pattern.test(inputPassword);
    return isValidPassword ? undefined : 'Пароль должен содержать как минимум одну заглавную букву и одну цифру и быть не менее 8 символов'
  }

  const validateCreditCardNumber = cardNumber => {
    const cardValidator = require('card-validator');
    return cardValidator.number(cardNumber).isValid ? undefined : 'Неверный номер карты'
  }

  const composeValidators = (...validators) => value => 
    validators.reduce((error, validator) => error || validator(value), undefined);

  const AdaptedInput = adapt(Input);
  const AdaptedInputPassword = adapt(Input, {
    type: 'password'
  });

  const AdaptedInputEmail = adapt(Input, {
    type: 'email'
  });

  let form = (
    <Form 
      onSubmit={signInHandler}
      render={({handleSubmit, form, submitting, pristine}) => (
        <form onSubmit={handleSubmit}>
          <FormGroup>
            <Label>Номер телефона</Label>
            <Field 
              name="phoneNumber" 
              validate={composeValidators(required, validatePhoneNumber)}
              component={AdaptedInput}
              placeholder="Номер телефона"
              control
            />
            <Error name="phoneNumber" />
          </FormGroup>
          <FormGroup>
            <Label>Пароль</Label>
            <Field
              name="password" 
              validate={composeValidators(required, validatePassword)}
              component={AdaptedInputPassword}
              placeholder="Пароль"
              control />
              <Error name="password" />
          </FormGroup>
          <Button 
            style={{backgroundColor: '#52DC22', margin: '15px'}}
            type="submit" 
            disabled={submitting || pristine}>
            Войти
          </Button>
          <Label style={{display: 'block'}} >
            Ещё нет аккаунта? <span onClick={switchAuthModeHandler} style={{cursor: 'pointer'}}>Зарегистрируйтесь</span>
          </Label>
        </form>
      )}
    />
  );

  if (isSignUp) {
    form = (
      <Form 
      onSubmit={signUpHandler}
      render={({handleSubmit, form, submitting, pristine}) => (
        <form onSubmit={handleSubmit}>
          <FormGroup>
            <Label>Имя</Label>
            <Field 
              name="name" 
              validate={composeValidators(required, validateUsername)}
              component={AdaptedInput}
              placeholder="Имя"
              control
            />
            <Error name="name" />
          </FormGroup>
          <FormGroup>
            <Label>Номер телефона</Label>
            <Field 
              name="phoneNumber" 
              validate={composeValidators(required, validatePhoneNumber)}
              component={AdaptedInput}
              placeholder="Номер телефона"
              control
            />
            <Error name="phoneNumber" />
          </FormGroup>
          <FormGroup>
            <Label>Электронный адрес</Label>
            <Field 
              name="email" 
              validate={composeValidators(required, validateEmail)}
              component={AdaptedInputEmail}
              placeholder="Адрес почтового ящика"
              control
            />
            <Error name="email" />
          </FormGroup>
          <FormGroup>
            <Label>Пароль</Label>
            <Field
              name="password" 
              validate={composeValidators(required, validatePassword)}
              component={AdaptedInputPassword}
              placeholder="Пароль"
              control />
              <Error name="password" />
          </FormGroup>
          <FormGroup>
            <Label>Банковская карта</Label>
            <Field
              name="creditCard" 
              validate={composeValidators(required, validateCreditCardNumber)}
              component={AdaptedInput}
              placeholder="Номер банковской карты"
              control />
              <Error name="creditCard" />
          </FormGroup>
          <Button 
            style={{backgroundColor: '#52DC22', margin: '15px'}}
            type="submit" 
            disabled={submitting || pristine}>
            Зарегистрироваться
          </Button>
          <Label style={{display: 'block'}} >
            Уже есть аккаунт? <span onClick={switchAuthModeHandler} style={{cursor: 'pointer'}}>Войдите</span>
          </Label>
        </form>
      )}
    />
    );
  }

  return (
    <div className={classes.Auth}>
      <div className={classes.Logo}>
        <img src={printedLogo} alt="Printed Logo"/>
      </div>
      {form}
      {props.isAuthenticated ? <Redirect to="/profile" /> : null}
      {!props.isAuthenticated && props.id ? <Redirect to="/create-spot" />: null}
    </div>
  );  
}

const mapStateToProps = state => {
  return {
    isAuthenticated: state.auth.isAuthenticated,
    id: state.auth.id
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onSignIn: loginData => dispatch(actions.signIn(loginData)),
    onSignUp: registrationData => dispatch(actions.signUp(registrationData))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(withErrorHandler(Auth, axios));