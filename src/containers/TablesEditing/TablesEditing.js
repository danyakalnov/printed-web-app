import React, { useState, useEffect } from 'react';
import MaterialTable from 'material-table';
import classes from './TablesEditing.module.css';
import axios from '../../axios-printed';
import { connect } from 'react-redux';

const TablesEditing = props => {

  const [ordersState, setOrdersState] = useState({
    columns: [
      { title: 'Id', field: 'id', editable: 'never' },
      { title: 'Cost', field: 'cost', editable: 'never'},
      { 
        title: 'CreatedAt',
        field: 'createdAt',
        type: 'datetime',
        editable: 'never'
      },
      { 
        title: 'DoneAt',
        field: 'doneAt',
        type: 'datetime',
        editable: 'never'
      },
      { 
        title: 'ReceivedAt',
        field: 'receivedAt',
        type: 'datetime',
        editable: 'never'
      },
      { 
        title: 'ReceiveOption', 
        field: 'receiveOption', 
        lookup: { 'university': 'university', 'personal': 'personal', 'dormitory': 'dormitory' }
      },
      { 
        title: 'Status',
        field: 'status',
        lookup: { 'placed': 'placed', 'ready': 'ready', 'inwork': 'inwork', 'received': 'received' }
      },
      { title: 'ClientId', field: 'clientId', type: 'numeric', editable: 'never' },
      { title: 'SpotId', field: 'spotId', type: 'numeric' },
      { title: 'Radius', field: 'radius', type: 'numeric' }
    ],

    data: []
  });

  const getOrdersData = async token => {
    axios.get('/orders', { headers: {Authorization: `${props.token}`}})
    .then(response => {
      const ordersData = response.data;
      setOrdersState(prevState => {
        return {
          ...prevState,
          data: ordersData
        }
      });
    })
    .catch(error => console.log(error));
  };

  const deleteOrder = async orderId => {
    axios.delete(`/orders/${orderId}`, { headers: {Authorization: `${props.token}`}})
      .then(
        response => console.log(response)
      )
      .catch(error => {
        console.log(error);
        getOrdersData(props.token);
      });
  };

  const createOrder = async inputData => {
    const createOrderData = {
      location : [0, 0],
      radius: +inputData.radius,
      receiveOption: inputData.receiveOption
    };

    let response = await fetch('https://printed-server.herokuapp.com/orders/new', {
      method: 'POST',
      headers: {
      'Authorization': `${props.token}`
      }
    });
    let newOrderId = await response.json();
    axios.put(`/orders/${newOrderId}`, createOrderData, { headers: {"Authorization": `${props.token}`}})
      .then(response => console.log(response))
      .catch(error => {
        console.log(error);
        getOrdersData(props.token);
      });
  };

  const setNewStatus = async inputData => {
    console.log(inputData);
    let data;
    if (inputData.newStatus !== 'inwork') {
      data = {
        status: inputData.newStatus
      };
    }

    else {
      data = {
        status: inputData.newStatus,
        spotId: inputData.spotId
      }
    }

    axios.patch(`/orders/${inputData.orderId}`, data, { headers: {Authorization: `${props.token}`}})
      .then(response => console.log(response))
      .catch(error => {
        console.log(error);
        getOrdersData(props.token);
      });
  };

  useEffect(() => {
    // Сделать запрос на получение данных из двух таблиц: Spots, Orders
    getOrdersData(props.token);
  }, []);

  console.log(ordersState.data);

  return (
    <div className={classes.Table}>
      <MaterialTable
      title="Orders Table"
      columns={ordersState.columns}
      data={ordersState.data}
      actions={[
        {
          icon: 'refresh',
          tooltip: 'Refresh Data',
          isFreeAction: true,
          onClick: () => getOrdersData(props.token)
        }
      ]}
      editable={{
        onRowAdd: newData =>
          new Promise(resolve => {
            setTimeout(() => {
              resolve();
              createOrder(newData);
              setOrdersState(prevState => {
                const data = [...prevState.data];
                data.push(newData);
                return { ...prevState, data };
              });
            }, 600);
          }),
        onRowUpdate: (newData, oldData) =>
          new Promise(resolve => {
            setTimeout(() => {
              resolve();
              if (oldData) {
                const sentData = {
                  newStatus: newData.status,
                  orderId: oldData.id
                };
                if (newData.spotId !== oldData.spotId && oldData.spotId === null) {
                  sentData.spotId = newData.spotId;
                  if (newData.status !== 'inwork') {
                    newData.spotId = oldData.spotId;
                  }
                }
                else {
                  newData.spotId = oldData.spotId;
                }
                console.log(sentData);
                setNewStatus(sentData);
                setOrdersState(prevState => {
                  const data = [...prevState.data];
                  data[data.indexOf(oldData)] = newData;
                  return { ...prevState, data };
                });
              }
            }, 600);
          }),
        onRowDelete: oldData =>
          new Promise(resolve => {
            setTimeout(() => {
              resolve();
              deleteOrder(oldData.id);
              setOrdersState(prevState => {
                const data = [...prevState.data];
                data.splice(data.indexOf(oldData), 1);
                return { ...prevState, data };
              });
            }, 600);
          }),
      }}
      />
    </div>
  );
};

const mapStateToProps = state => {
  return {
    token: state.auth.token
  };
};

export default connect(mapStateToProps, null)(TablesEditing);