export const updateObject = (oldObject, updatedProperties) => {
  return {
    ...oldObject,
    ...updatedProperties
  }
};

export const modifyObject = (oldObject, newPropertyName, newPropertyValue) => {
  return {
    ...oldObject,
    [newPropertyName]: newPropertyValue
  };
};