import * as actionTypes from '../actions/actionTypes';
import { updateObject } from '../utility';

const initialState = {
  address: '',
  status: null,
  statusEditingError: null,
  location: null,
  name: '',
  creatingError: null,
  getSpotError: null,
  spotId: null,
};

const createSpotSuccess = (state, action) => {
  return updateObject(state, {
    address: action.address,
    status: action.status,
    location: action.location
  });
};

const createSpotFail = (state, action) => {
  return updateObject(state, {
    creatingError: action.creatingError
  });
};

const getSpotSuccess = (state, action) => {
  return updateObject(state, {
    spotId: action.spotId,
    address: action.address,
    status: action.status,
    name: action.name
  });
};

const getSpotFail = (state, action) => {
  return updateObject(state, {
    getSpotError: action.getSpotError
  });
};

const editStatusSuccess = (state, action) => {
  return updateObject(state, {
    status: action.status
  });
};

const editStatusFail = (state, action) => {
  return updateObject(state, {
    statusEditingError: action.statusEditingError
  });
};

const reducer = (state = initialState, action) => {
  switch(action.type){
    case actionTypes.CREATE_SPOT_SUCCESS: return createSpotSuccess(state, action);
    case actionTypes.CREATE_SPOT_FAIL: return createSpotFail(state, action);
    case actionTypes.GET_SPOT_SUCCESS: return getSpotSuccess(state, action);
    case actionTypes.GET_SPOT_FAIL: return getSpotFail(state, action);
    case actionTypes.EDIT_STATUS_SUCCESS: return editStatusSuccess(state, action);
    case actionTypes.EDIT_STATUS_FAIL: return editStatusFail(state, action);
    default: 
      return state;
  }
};

export default reducer;