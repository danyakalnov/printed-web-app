import * as actionTypes from '../actions/actionTypes';
import { updateObject } from '../utility';

const initialState = {
  token: null,
  expire: null,
  phoneNumber: null,
  phoneEditError: null,
  name: null,
  id: null,
  email: null,
  emailEditError: null,
  accountNumber: null,
  signInError: false,
  signUpError: false, 
  isAuthenticated: false,
};

const signInSuccess = (state, action) => {
  return updateObject(state, {
    token: action.token,
    expire: action.expire,
    phoneNumber: action.phoneNumber,
    name: action.name,
    id: action.id,
    email: action.email,
    accountNumber: action.accountNumber,
    isAuthenticated: action.isAuthenticated,
  });
};

const signUpSuccess = (state, action) => {
  console.log('SIGN_UP reducer');
  console.log(action.token);
  console.log(action.expire);
  return updateObject(state, {
    token: action.token,
    expire: action.expire,
    id: action.id,
    name: action.name,
    email: action.email,
    phoneNumber: action.phoneNumber,
    accountNumber: action.accountNumber,
    isAuthenticated: action.isAuthenticated
  });
};

const signInFail = (state, action) => {
  return updateObject(state, {
    signInError: action.signInError
  });
};

const signUpFail = (state, action) => {
  return updateObject(state, {
    signUpError: action.signInError
  });
};

const setSuccessAuthentication = (state, action) => {
  return updateObject(state, {
    isAuthenticated: action.isAuthenticated
  });
};

const logout = (state, action) => {
  return updateObject(state, {
    token: null,
    expire: null,
    phoneNumber: null,
    name: null,
    id: null,
    email: null,
    accountNumber: null,
    signInError: false,
    signUpError: false, 
    isAuthenticated: false,
  });
};

const editPhoneSuccess = (state, action) => {
  return updateObject(state, {
    phoneNumber: action.phoneNumber
  });
};

const editPhoneFail = (state, action) => {
  return updateObject(state, {
    phoneEditError: action.phoneEditError
  });
};

const editEmailSuccess = (state, action) => {
  return updateObject(state, {
    email: action.email
  });
};

const editEmailFail = (state, action) => {
  return updateObject(state, {
    emailEditError: action.emailEditError
  });
};

const reducer = (state = initialState, action) => {
  switch(action.type) {
    case actionTypes.SIGN_IN_SUCCESS: return signInSuccess(state, action);
    case actionTypes.SIGN_IN_FAIL: return signInFail(state, action);
    case actionTypes.SIGN_UP_SUCCESS: return signUpSuccess(state, action);
    case actionTypes.SIGN_UP_FAIL: return signUpFail(state, action);
    case actionTypes.LOGOUT: return logout(state, action);
    case actionTypes.SET_SUCCESS_AUTHENTICATION: return setSuccessAuthentication(state, action);
    case actionTypes.EDIT_PHONE_SUCCESS: return editPhoneSuccess(state, action);
    case actionTypes.EDIT_PHONE_FAIL: return editPhoneFail(state, action);
    case actionTypes.EDIT_EMAIL_SUCCESS: return editEmailSuccess(state, action);
    case actionTypes.EDIT_EMAIL_FAIL: return editEmailFail(state, action);
    default:
      return state;
  }
};

export default reducer;