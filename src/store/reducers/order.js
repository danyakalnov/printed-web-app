import * as actionTypes from '../actions/actionTypes';
import { updateObject, modifyObject } from '../utility';
import produce from 'immer';

const initialState = {
  ordersPool: [],
  fetchOrdersError: null,
  getOrderDocumentsError: null,
  inworkOrders: [],
  readyOrders: [],
  receivedOrders: [],
  setOrderStatusFail: null
};

const fetchOrdersPoolSuccess = (state, action) => {
  return updateObject(state, {
    ordersPool: action.ordersPool
  });
};

const fetchOrdersPoolFail = (state, action) => {
  return updateObject(state, {
    fetchOrdersPoolError: action.fetchOrdersPoolError
  });
};

const getOrderDocumentsSuccess = (state, action) => {
  const orderIndex = state[`${action.poolName}`].findIndex(order => order.id === action.orderId);
  const orderToAddDocuments = {
    ...state[`${action.poolName}`][orderIndex]
  };
  const orderWithDocuments = modifyObject(orderToAddDocuments, 'documents', action.orderDocuments);
  return produce (state, draft => {
    draft[`${action.poolName}`][orderIndex] = orderWithDocuments;
  }); 
};

const getOrderDocumentsFail = (state, action) => {
  return updateObject(state, {
    getOrderDocumentsError: action.getOrderDocumentsError
  });
};

const setOrderStatusSuccess = (state, action) => {
  const orderIndex = state.ordersPool.findIndex(order => order.id === action.orderId);
  const orderToUpdateStatus = {
    ...state.ordersPool[orderIndex]
  };

  let orderWithNewStatus;

  if (action.status === 'inwork') {
    orderWithNewStatus = updateObject(orderToUpdateStatus, {
      status: action.status,
      spotId: action.spotId
    });
  }

  else {
    orderWithNewStatus = updateObject(orderToUpdateStatus, {
      status: action.status
    });
  }

  return produce(state, draft => {
    draft.ordersPool[orderIndex] = orderWithNewStatus;
  });
};

const setOrderStatusFail = (state, action) => {
  return updateObject(state, {
    setOrderStatusFail: action.setOrderStatusFail
  });
};

const getOrdersWithStatusSuccess = (state, action) => {
  const ordersKey = Object.keys(action).find(key => key.includes('Orders'));
  return updateObject(state, {
    [ordersKey]: action[ordersKey]
  });
};

const getOrdersWithStatusFail = (state, action) => {
  return updateObject(state, {
    fetchOrdersError: action.fetchOrdersError
  });
};

const cleanupOrdersPool = (state, action) => {
  const poolNameKey = Object.keys(action).find(key => key.includes('Orders') || key.includes('orders'));
  return updateObject(state, {
    [poolNameKey]: action[poolNameKey]
  });
};

const reducer = (state = initialState, action) => {
  switch(action.type){
    case actionTypes.FETCH_ORDERS_POOL_SUCCESS: return fetchOrdersPoolSuccess(state, action);
    case actionTypes.FETCH_ORDERS_POOL_FAIL: return fetchOrdersPoolFail(state, action);
    case actionTypes.GET_ORDER_DOCUMENTS_SUCCESS: return getOrderDocumentsSuccess(state, action);
    case actionTypes.GET_ORDER_DOCUMENTS_FAIL: return getOrderDocumentsFail(state, action);
    case actionTypes.SET_ORDER_STATUS_SUCCESS: return setOrderStatusSuccess(state, action);
    case actionTypes.SET_ORDER_STATUS_FAIL: return setOrderStatusFail(state, action);
    case actionTypes.GET_ORDERS_WITH_STATUS_SUCCESS: return getOrdersWithStatusSuccess(state, action);
    case actionTypes.GET_ORDERS_WITH_STATUS_FAIL: return getOrdersWithStatusFail(state, action);
    case actionTypes.CLEANUP_ORDERS_POOL: return cleanupOrdersPool(state, action);
    default: 
      return state;
  }
};

export default reducer;