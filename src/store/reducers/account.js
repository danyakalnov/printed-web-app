import * as actionTypes from '../actions/actionTypes';
import { updateObject } from '../utility';

const initialState = {
  balance: null,
  rememberCard: false,
  cardNumberCut: null,
  getAccountError: null
};

const getAccountSuccess = (state, action) => {
  return updateObject(state, {
    balance: action.balance,
    rememberCard: action.rememberCard,
    cardNumberCut: action.cardNumberCut
  });
};

const getAccountFail = (state, action) => {
  return updateObject(state, {
    getAccountError: action.getAccountError
  });
};

const reducer = (state = initialState, action) => {
  switch(action.type){
    case actionTypes.GET_ACCOUNT_SUCCESS: return getAccountSuccess(state, action);
    case actionTypes.GET_ACCOUNT_FAIL: return getAccountFail(state, action);
    default: 
      return state;
  }
};

export default reducer;