import axios from '../../axios-printed';
import * as actionTypes from './actionTypes';

export const createSpotSuccess = spotData => {
  return {
    type: actionTypes.CREATE_SPOT_SUCCESS,
    address: spotData.address,
    location: spotData.location,
    status: spotData.status
  };
};

export const createSpotFail = error => {
  return {
    type: actionTypes.CREATE_SPOT_FAIL,
    creatingError: error
  };
};

export const createSpot = spotData => {
  return dispatch => {
    const createSpotURL = '/spots';

    const spotDataToBeSent = {
      address: spotData.address,
      location: spotData.location,
      status: spotData.status
    };

    axios.post(createSpotURL, spotDataToBeSent, { headers: {Authorization: `${spotData.token}`}})
      .then(response => {
        dispatch(createSpotSuccess(spotData));
      })
      .catch(error => dispatch(createSpotFail(error)));
  };
};

export const getSpotSuccess = spotData => {
  return {
    type: actionTypes.GET_SPOT_SUCCESS,
    spotId: spotData.spotId,
    address: spotData.address,
    status: spotData.status,
    name: spotData.name
  };
};

export const getSpotFail = error => {
  return {
    type: actionTypes.GET_SPOT_FAIL,
    getSpotError: error
  };
};

export const getSpot = inputData => {
  return dispatch => {
    const getSpotURL = `/spots?adminId=${inputData.userId}`;

    axios.get(getSpotURL, { headers: {Authorization: `${inputData.token}`}})
      .then(response => {

        const spotData = {
          spotId: response.data[0].id,
          address: response.data[0].address,
          status: response.data[0].status,
          name: response.data[0].name
        };
        localStorage.setItem('spotId', response.data[0].id);
        dispatch(getSpotSuccess(spotData));
      })
      .catch(error => {
        dispatch(getSpotFail(error));
      });
  };
};

export const editStatusSuccess = newSpotStatus => {
  return {
    type: actionTypes.EDIT_STATUS_SUCCESS,
    status: newSpotStatus
  };
};

export const editStatusFail = error => {
  return {
    type: actionTypes.EDIT_STATUS_FAIL,
    statusEditingError: error
  };
};

export const editStatus = inputData => {
  return dispatch => {
    const editStatusURL = `/spots/${inputData.spotId}`;

    axios.patch(editStatusURL, inputData.newStatus, { headers: {Authorization: `${inputData.token}`}})
      .then(response => {
        if (response.status === 204) {
          dispatch(editStatusSuccess(inputData.newStatus.status));
        }
      })
      .catch(error => dispatch(editStatusFail(error)));
  };
};