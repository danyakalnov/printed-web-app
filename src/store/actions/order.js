import axios from '../../axios-printed';
import * as actionTypes from './actionTypes';

export const fetchOrdersPoolSuccess = ordersPool => {
  return {
    type: actionTypes.FETCH_ORDERS_POOL_SUCCESS,
    ordersPool: ordersPool
  };
};

export const fetchOrdersPoolFail = error => {
  return {
    type: actionTypes.FETCH_ORDERS_POOL_FAIL,
    fetchOrdersPoolError: error
  };
};

export const fetchOrdersPool = token => {
  return dispatch => {
    const getOrdersPoolURL = '/orders/placed';

    axios.get(getOrdersPoolURL, { headers: {Authorization: `${token}`}})
      .then(response => {
        dispatch(fetchOrdersPoolSuccess(response.data));
      })
      .catch(error => dispatch(fetchOrdersPoolFail(error)));
  };
};

export const getOrdersWithStatusFail = inputData => {
  return {
    type: actionTypes.GET_ORDERS_WITH_STATUS_FAIL,
    fetchOrdersError: inputData.error
  };
};

export const getOrdersWithStatusSuccess = inputData => {
  return {
    type: actionTypes.GET_ORDERS_WITH_STATUS_SUCCESS,
    ...inputData
  };
};

export const getOrdersWithStatus = inputData => {
  return dispatch => {
    const getOrdersWithStatusURL = `/spots/${inputData.spotId}/orders?status=${inputData.status}`;

    axios.get(getOrdersWithStatusURL, { headers: {Authorization: `${inputData.token}`}})
      .then(response => {
        dispatch(getOrdersWithStatusSuccess({
          [inputData.status+'Orders']: response.data
        }));
      })
      .catch(error => dispatch(getOrdersWithStatusFail(error)));
  };
};

export const getOrderDocumentsSuccess = inputData => {
  return {
    type: actionTypes.GET_ORDER_DOCUMENTS_SUCCESS,
    orderDocuments: inputData.documents,
    orderId: inputData.orderId,
    poolName: inputData.poolName
  };
};

export const getOrderDocumentsFail = error => {
  return {
    type: actionTypes.GET_ORDER_DOCUMENTS_FAIL,
    getOrderDocumentsError: error
  };
};

export const getOrderDocuments = inputData => {
  return dispatch => {
    const getOrderDocumentsURL = `/orders/${inputData.orderId}/documents`;

    axios.get(getOrderDocumentsURL, { headers: {Authorization: `${inputData.token}`}})
      .then(response => {
        const outputData = {
          poolName: inputData.poolName,
          documents: response.data,
          orderId: inputData.orderId
        };
        dispatch(getOrderDocumentsSuccess(outputData));
      })
      .catch(error => {
        dispatch(getOrderDocumentsFail(error))
      });
  };
};

export const setOrderStatusSuccess = inputData => {
  return {
    type: actionTypes.SET_ORDER_STATUS_SUCCESS,
    status: inputData.newStatus,
    orderId: inputData.orderId,
    spotId: inputData.spotId
  };
};

export const setOrderStatusFail = inputData => {
  return {
    type: actionTypes.SET_ORDER_STATUS_FAIL,
    setOrderStatusFail: inputData.error
  };
};

export const setOrderStatus = inputData => {
  return dispatch => {
    const setOrderStatusURL = `/orders/${inputData.orderId}`;

    const patchData = {
      status: inputData.newStatus
    };
    if (inputData.newStatus === 'inwork') {
      patchData.spotId = inputData.spotId;
    }

    axios.patch(setOrderStatusURL, patchData, { headers: {Authorization: `${inputData.token}`}})
      .then(response => {
        if (response.status === 204) {
          dispatch(setOrderStatusSuccess({
            newStatus: inputData.newStatus,
            orderId: inputData.orderId,
            spotId: inputData.spotId
          }));
        }
      })
      .catch(error => dispatch(setOrderStatusFail(error)));
  };
};

export const cleanupOrdersPool = (poolName) => {
  return {
    type: actionTypes.CLEANUP_ORDERS_POOL,
    [poolName]: []
  };
};