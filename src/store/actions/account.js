import axios from '../../axios-printed';
import * as actionTypes from './actionTypes';

export const getAccountSuccess = accountData => {
  return {
    type: actionTypes.GET_ACCOUNT_SUCCESS,
    balance: accountData.balance,
    rememberCard: accountData.rememberCard,
    cardNumberCut: accountData.cardNumberCut
  };
};

export const getAccountFail = error => {
  return {
    type: actionTypes.GET_ACCOUNT_FAIL,
    getAccountError: error
  };
};

export const getAccount = inputData => {
  return dispatch => {
    const getAccountURL = `/accounts/${inputData.accountNumber}`;
  
    axios.get(getAccountURL, { headers: {Authorization: `${inputData.token}`}})
      .then(response => {
        const accountData = {
          balance: response.data.balance,
          rememberCard: response.data.rememberCard,
          cardNumberCut: response.data.cardNumberCut
        };

        dispatch(getAccountSuccess(accountData));
      })
      .catch(error => {
        dispatch(getAccountFail(error));
      });
  };
};