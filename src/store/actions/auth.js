import axios from '../../axios-printed';
import * as actionTypes from './actionTypes';

export const signUpSuccess = authData => {
  return {
    type: actionTypes.SIGN_UP_SUCCESS,
    token: authData.token,
    expire: authData.expire,
    id: authData.id,
    name: authData.name,
    email: authData.email,
    phoneNumber: authData.phoneNumber,
    accountNumber: authData.accountNumber,
    isAuthenticated: authData.isAuthenticated
  };
};

export const setSuccessAuthentication = () => {

  localStorage.setItem('isAuthenticated', true);

  return {
    type: actionTypes.SET_SUCCESS_AUTHENTICATION,
    isAuthenticated: true
  };
};

export const signInSuccess = authData => {
  return {
    type: actionTypes.SIGN_IN_SUCCESS,
    token: authData.token,
    expire: authData.expire,
    phoneNumber: authData.phoneNumber,
    name: authData.name,
    id: authData.id,
    email: authData.email,
    accountNumber: authData.accountNumber,
    isAuthenticated: authData.isAuthenticated
  };
};

export const signUpFail = error => {
  return {
    type: actionTypes.SIGN_UP_FAIL,
    signUpError: error
  };
};

export const signInFail = error => {
  return {
    type: actionTypes.SIGN_IN_FAIL,
    signInError: error
  };
};

export const logout = () => {
  localStorage.removeItem('token');
  localStorage.removeItem('expire');
  localStorage.removeItem('userId');
  localStorage.removeItem('accountNumber');
  localStorage.removeItem('name');
  localStorage.removeItem('email');
  localStorage.removeItem('phoneNumber');
  localStorage.removeItem('isAuthenticated');

  return {
      type: actionTypes.LOGOUT
  };
};

export const checkAuthTimeout = expirationTime => {
  return dispatch => {
      setTimeout(() => {
          dispatch(logout());
      }, expirationTime);
  };
};

export const signIn = loginData => {
  return dispatch => {
    let updatedAuthData;

    const signInURL = '/login';

    axios.post(signInURL, loginData)
      .then(response => {

        console.log('Get response from server');

        const expirationDate = new Date(new Date().getTime() + response.data.expire);
        localStorage.setItem('token', `Bearer_${response.data.token}`);
        localStorage.setItem('expire', expirationDate);
        localStorage.setItem('userId', response.data.id);
        localStorage.setItem('phoneNumber', loginData.phoneNumber);
        localStorage.setItem('name', response.data.name);
        localStorage.setItem('email', response.data.email);
        localStorage.setItem('accountNumber', response.data.accountNumber);
        localStorage.setItem('isAuthenticated', true);

        updatedAuthData = {
          token: `Bearer_${response.data.token}`,
          expire: expirationDate,
          phoneNumber: loginData.phoneNumber,
          name: response.data.name,
          id: response.data.id,
          email: response.data.email,
          accountNumber: response.data.accountNumber,
          isAuthenticated: true
        };

        dispatch(signInSuccess(updatedAuthData));
        dispatch(checkAuthTimeout(response.data.expire));
      })
      .catch(error => {
        dispatch(signInFail(error));
      });
    };

};

export const signUp = registrationData => {
  return dispatch => {
    let updatedAuthData;

    const signUpURL = '/signup';

    axios.post(signUpURL, registrationData)
      .then(response => {
        console.log(response.data);
        const expirationDate = new Date(new Date().getTime() + response.data.expire);

        localStorage.setItem('token', `Bearer_${response.data.token}`);
        localStorage.setItem('expire', expirationDate);
        localStorage.setItem('userId', response.data.id);
        localStorage.setItem('phoneNumber', registrationData.phoneNumber);
        localStorage.setItem('name', response.data.name);
        localStorage.setItem('email', response.data.email);
        localStorage.setItem('accountNumber', response.data.accountNumber);

        updatedAuthData = {
          token: `Bearer_${response.data.token}`,
          expire: expirationDate,
          id: response.data.id,
          name: response.data.name,
          email: response.data.email,
          phoneNumber: response.data.phoneNumber,
          accountNumber: response.data.accountNumber,
          isAuthenticated: false
        };
        console.log('SIGN_UP ACTION');
        console.log(updatedAuthData);

        dispatch(signUpSuccess(updatedAuthData));
        dispatch(checkAuthTimeout(response.data.expire));
      })
      .catch(error => {
        dispatch(signUpFail(error));
      });
  };
};

export const authCheckState = () => {
  return dispatch => {
      const token = localStorage.getItem('token');
      if (!token) {
          dispatch(logout());
      } 

      else {
        const expirationDate = new Date(localStorage.getItem('expire'));
        if (expirationDate <= new Date()) {
            dispatch(logout());
        } 
        
        else {
          const updatedAuthData = {
            token: token,
            expire: expirationDate,
            id: localStorage.getItem('userId'),
            phoneNumber: localStorage.getItem('phoneNumber'),
            name: localStorage.getItem('name'),
            email: localStorage.getItem('email'),
            accountNumber: localStorage.getItem('accountNumber'),
            isAuthenticated: localStorage.getItem('isAuthenticated')
          };

          dispatch(signInSuccess(updatedAuthData));
          dispatch(checkAuthTimeout(expirationDate.getTime() - new Date().getTime()));
        }   
      }
  };
};

export const editPhoneSuccess = newPhoneNumber => {
  return {
    type: actionTypes.EDIT_PHONE_SUCCESS,
    phoneNumber: newPhoneNumber
  };
};

export const editPhoneFail = error => {
  return {
    type: actionTypes.EDIT_PHONE_FAIL,
    phoneEditError: error
  };
};

export const editPhone = inputData => {
  return dispatch => {
    const editPhoneURL = `/users/${inputData.userId}`;

    axios.patch(editPhoneURL, inputData.personalData, { headers: {Authorization: `${inputData.token}`}})
      .then(response => {
        if (response.status === 200) {
          localStorage.removeItem('phoneNumber');
          localStorage.setItem('phoneNumber', inputData.personalData.phoneNumber);

          localStorage.removeItem('token');
          localStorage.setItem('token', `Bearer_${response.data}`);
          dispatch(editPhoneSuccess(inputData.personalData.phoneNumber));
        }
      })
      .catch(error => dispatch(editPhoneFail(error)));
  };
};

export const editEmailSuccess = newEmail => {
  return {
    type: actionTypes.EDIT_EMAIL_SUCCESS,
    email: newEmail
  };
};

export const editEmailFail = error => {
  return {
    type: actionTypes.EDIT_EMAIL_FAIL,
    emailEditError: error
  };
};

export const editEmail = inputData => {
  return dispatch => {
    const editEmailURL = `/users/${inputData.userId}`;

    axios.patch(editEmailURL, inputData.email, { headers: {Authorization: `${inputData.token}`}})
      .then(response => {
        if (response.status === 204) {
          localStorage.removeItem('email');
          localStorage.setItem('email', inputData.email.email);
          dispatch(editEmailSuccess(inputData.email.email));
        }
      })
      .catch(error => dispatch(editEmailFail(error)));
  };
};