import React, { Component } from "react";

import { Switch, Route, Redirect, withRouter } from "react-router-dom";
import { connect } from "react-redux";
import Auth from "./containers/Auth/Auth";
import SpotRegister from "./containers/Auth/SpotRegister/SpotRegister";
import Logout from "./containers/Auth/Logout/Logout";
import Profile from "./components/Profile/Profile";
import SideBar from "./components/UI/SideBar/SideBar";

import './App.css';
import { MainLayout } from './components/UI/MainLayout/MainLayout';

import OrdersPool from "./components/Orders/OrdersPool/OrdersPool";

import OrdersList from "./components/Orders/OrdersList/OrdersList";

import TablesEditing from "./containers/TablesEditing/TablesEditing";

import * as actions from "./store/actions/index";

class App extends Component {
  componentDidMount() {
    this.props.onTryAutoSignup();
  }

  render() {
    let routes = (
      <Switch>
        <Route path="/" exact component={Auth} />
        <Redirect to="/" />
      </Switch>
    );

    if (this.props.isAuthenticated) {
      routes = (
        <Switch>
          <Route
            key="inwork-orders"
            path="/inwork-orders"
            render={props => (
              <OrdersList
                {...props}
                poolName="inworkOrders"
                poolTitle="Заказы в работе"
                noOrdersMessage="Нет заказов в работе :("
              />
            )}
          />
          <Route
            key="completed-orders"
            path="/completed-orders"
            render={props => (
              <OrdersList
                {...props}
                poolName="readyOrders"
                poolTitle="Выполненные заказы"
                noOrdersMessage="Вы не выполнили ни одного заказа"
              />
            )}
          />
          <Route
            key="delivered-orders"
            path="/delivered-orders"
            render={props => (
              <OrdersList
                {...props}
                poolName="receivedOrders"
                poolTitle="Доставленные заказы"
                noOrdersMessage="Нет доставленных заказов"
              />
            )}
          />
          <Route path="/orders" exact component={OrdersPool} />
          <Route path="/profile" exact component={Profile} />
          <Route path="/tables-editing" exact component={TablesEditing} />
          <Route path="/logout" component={Logout} />
          <Redirect to="/profile" />
        </Switch>
      );
    }

    if (this.props.id && !this.props.isAuthenticated) {
      routes = (
        <Switch>
          <Route path="/create-spot" component={SpotRegister} />
          <Route path="/" exact component={Auth} />
          <Redirect to="/" />
        </Switch>
      );
    }

    return (
      <div className="App">
        {this.props.isAuthenticated && <SideBar />}
        <MainLayout>
          {routes}
        </MainLayout>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    isAuthenticated: state.auth.isAuthenticated,
    id: state.auth.id
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onTryAutoSignup: () => dispatch(actions.authCheckState())
  };
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(App));
