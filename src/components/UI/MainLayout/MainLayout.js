import styled from 'styled-components';

export const MainLayout = styled.div`
  padding: 10px 40px;
  flex-grow: 1;
  align-self: flex-start;
`;