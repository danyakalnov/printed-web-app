import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faFileAlt } from '@fortawesome/free-solid-svg-icons';

const documentIcon = props => (
  <FontAwesomeIcon icon={faFileAlt} size="2x" style={{
    color: 'EBE4CE',
    margin: '0 15px',
    marginRight: '6px'
  }}/>
);

export default documentIcon;