import React from 'react';
import { Field } from 'react-final-form';
import { ControlFeedback } from 'smooth-ui';

const error = ({ name }) => (
  <Field name={name} subscription={{ error: true, touched: true }}>
    {({ meta: { touched, error } }) =>
      touched && error ? (
        <ControlFeedback valid={!error}>{error}</ControlFeedback>
      ) : null
    }
  </Field>
);

export default error;