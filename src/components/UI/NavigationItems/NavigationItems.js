import React from 'react';
import classes from './NavigationItems.module.css';
import NavigationItem from './NavigationItem/NavigationItem';

const navigationItems = () => (
  <ul className={classes.NavigationItems}>
    <NavigationItem link="/profile" >Профиль</NavigationItem>
    <NavigationItem link="/orders" exact>Пул заказов</NavigationItem>
    <NavigationItem link="/inwork-orders" exact>Заказы в работе</NavigationItem>
    <NavigationItem link="/completed-orders" exact>Выполненные заказы</NavigationItem>
    <NavigationItem link="/delivered-orders" exact>Доставленные заказы</NavigationItem>
    <NavigationItem link="/logout" >Выйти</NavigationItem>
  </ul>
);

export default navigationItems;