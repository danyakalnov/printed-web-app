import React, { useEffect, useState } from "react";
import { connect } from "react-redux";
import classes from "./Profile.module.css";
import userPhoto from "../../assets/images/user.png";
import styled from 'styled-components';
import withErrorHandler from "../../hoc/withErrorHandler/withErrorHandler";

import { Form, Field } from "react-final-form";
import adapt from "../../hoc/Adapt/Adapt";
import Error from "../../components/UI/InputError/InputError";
import { Label, Input, FormGroup, Select } from "smooth-ui";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCircle, faPencilAlt } from "@fortawesome/free-solid-svg-icons";

import { parsePhoneNumberFromString } from "libphonenumber-js";

import axios from "../../axios-printed";
import * as actions from "../../store/actions/index";

const Profile = props => {
  useEffect(() => {
    // Get information about the money of the manager
    const inputData = {
      accountNumber: props.accountNumber,
      token: props.token
    };
    props.onGetAccount(inputData);
  }, [props.accountNumber]);

  useEffect(() => {
    // Get information about the spot of the manager
    const inputData = {
      userId: props.userId,
      token: props.token
    };
    props.onGetSpot(inputData);
  }, [props.userId]);

  const [isStatusEditing, setStatusEditing] = useState(false);
  const [isPhoneEditing, setPhoneEditing] = useState(false);
  const [isEmailEditing, setEmailEditing] = useState(false);

  const statusEditChangeHandler = () => {
    setStatusEditing(prev => !prev);
  };

  const phoneEditChangeHandler = () => {
    setPhoneEditing(prev => !prev);
  };

  const emailEditChangeHandler = () => {
    setEmailEditing(prev => !prev);
  };

  const emailSubmitHandler = email => {
    emailEditChangeHandler();
    console.log(email);
    const inputData = {
      email: {
        ...email
      },
      token: props.token,
      userId: props.userId
    };

    props.onEditEmail(inputData);
  };

  const phoneSubmitHandler = personalData => {
    const correctPhoneNumber = getFormattedPhoneNumber(
      personalData.phoneNumber
    );
    phoneEditChangeHandler();
    const inputData = {
      personalData: {
        phoneNumber: correctPhoneNumber,
        password: personalData.password
      },
      token: props.token,
      userId: props.userId
    };

    props.onEditPhone(inputData);
  };

  const statusSubmitHandler = status => {
    statusEditChangeHandler();

    const inputData = {
      newStatus: status,
      token: props.token,
      spotId: props.spotId
    };

    props.onEditSpotStatus(inputData);
  };

  const AdaptedSelect = adapt(Select);
  const AdaptedInput = adapt(Input);

  const AdaptedInputPassword = adapt(Input, {
    type: "password"
  });

  const getFormattedPhoneNumber = phoneNumber => {
    const phoneNum = parsePhoneNumberFromString(phoneNumber);
    return phoneNum.number;
  };

  const validatePhoneNumber = inputPhoneNumber => {
    const phoneNumber = parsePhoneNumberFromString(inputPhoneNumber);
    if (phoneNumber) {
      console.log(phoneNumber.isValid());
      return phoneNumber.isValid() &&
        getFormattedPhoneNumber(inputPhoneNumber) !== props.phoneNumber
        ? undefined
        : "Некорректный номер";
    }

    return undefined;
  };

  const validateEmail = email => {
    const pattern = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/;
    let isValidEmail = pattern.test(email);
    return isValidEmail && email !== props.email
      ? undefined
      : "Неверный e-mail";
  };

  const validatePassword = inputPassword => {
    const pattern = /^(?=.*[A-Z])(?=.*\d)[A-Za-z\d@$!%*?&]{8,}$/;
    let isValidPassword = pattern.test(inputPassword);
    return isValidPassword
      ? undefined
      : "Пароль должен содержать как минимум одну заглавную букву и одну цифру и быть не менее 8 символов";
  };

  const required = value => {
    return value ? undefined : "Обязательное поле";
  };

  const composeValidators = (...validators) => value =>
    validators.reduce(
      (error, validator) => error || validator(value),
      undefined
    );

  const statusEditingForm = (
    <Form
      onSubmit={statusSubmitHandler}
      render={({ handleSubmit, form, submitting, pristine }) => (
        <form onSubmit={handleSubmit} style={{ marginLeft: "5px" }}>
          <FormGroup style={{ margin: 0 }}>
            <Field
              style={{ width: "100px" }}
              name="status"
              type="select"
              component={AdaptedSelect}
              validate={required}
              arrow={false}
              disabled={!isStatusEditing}
              options={[
                { value: "free", label: "free" },
                { value: "busy", label: "busy" },
                { value: "inactive", label: "inactive" }
              ]}
              control
            />
            <button
              type="submit"
              style={{
                border: 0,
                padding: 0,
                marginLeft: "8px",
                display: "inline",
                backgroundColor: "white",
                cursor: "pointer",
                outline: "none"
              }}
            >
              Обновить статус
            </button>
            <Error name="status" />
          </FormGroup>
        </form>
      )}
    />
  );

  const phoneEditingForm = (
    <Form
      onSubmit={phoneSubmitHandler}
      render={({ handleSubmit, form, submitting, pristine }) => (
        <form onSubmit={handleSubmit} style={{ marginLeft: "5px" }}>
          <FormGroup style={{ margin: 0 }}>
            <Label>Новый номер телефона: </Label>
            <Field
              style={{ width: "200px" }}
              name="phoneNumber"
              component={AdaptedInput}
              placeholder={props.phoneNumber}
              validate={composeValidators(required, validatePhoneNumber)}
              control
            />
            <Error name="phoneNumber" />
          </FormGroup>
          <FormGroup>
            <Label>Подтвердите пароль: </Label>
            <Field
              name="password"
              validate={composeValidators(required, validatePassword)}
              component={AdaptedInputPassword}
              placeholder="Ваш пароль"
              control
            />
            <Error name="password" />
          </FormGroup>
          <button
            type="submit"
            style={{
              border: 0,
              padding: 0,
              marginLeft: "8px",
              display: "inline",
              backgroundColor: "white",
              cursor: "pointer",
              outline: "none"
            }}
          >
            Обновить номер телефона
          </button>
        </form>
      )}
    />
  );

  const emailEditingForm = (
    <Form
      onSubmit={emailSubmitHandler}
      render={({ handleSubmit, form, submitting, pristine }) => (
        <form onSubmit={handleSubmit} style={{ marginLeft: "5px" }}>
          <FormGroup style={{ margin: 0 }}>
            <Label>Новая почта: </Label>
            <Field
              style={{ width: "200px" }}
              name="email"
              component={AdaptedInput}
              placeholder={props.email}
              validate={composeValidators(required, validateEmail)}
              control
            />
            <Error name="email" />
          </FormGroup>
          <button
            type="submit"
            style={{
              border: 0,
              padding: 0,
              marginLeft: "8px",
              display: "inline",
              backgroundColor: "white",
              cursor: "pointer",
              outline: "none"
            }}
          >
            Обновить почтовый адрес
          </button>
        </form>
      )}
    />
  );

  let statusInputClasses = [classes.StatusIcon];

  if (props.spotStatus === "free") {
    statusInputClasses.push(classes.Green);
  }

  if (props.spotStatus === "busy") {
    statusInputClasses.push(classes.Orange);
  }

  if (props.spotStatus === "inactive") {
    statusInputClasses.push(classes.Red);
  }

  return (
    <React.Fragment>
      <div className={classes.UserInfo}>
        <h1 style={{ textAlign: "center", margin: "20px auto" }}>
          Мой аккаунт
        </h1>
        <div className={classes.Logo}>
          <img src={userPhoto} alt="user" />
        </div>
        <h2 className={classes.Username}>{props.name}</h2>
        <ProfileInfoContainer>
          <p>
            Баланс счёта: {props.balance} <span>₽</span>
          </p>
          <p>Адрес точки печати: {props.spotAddress}</p>
          <div
            style={{
              display: "flex",
              justifyContent: "flex-start",
              alignItems: "center"
            }}
          >
            <p>Статус точки печати: {props.spotStatus}</p>
            <FontAwesomeIcon
              icon={faCircle}
              className={statusInputClasses.join(" ")}
            />
            <FontAwesomeIcon
              icon={faPencilAlt}
              style={{ cursor: "pointer", marginLeft: "8px" }}
              onClick={statusEditChangeHandler}
            />
          </div>
          {isStatusEditing ? statusEditingForm : null}
          <div
            style={{
              display: "flex",
              justifyContent: "flex-start",
              alignItems: "center"
            }}
          >
            <p>Номер телефона: {props.phoneNumber}</p>
            <FontAwesomeIcon
              icon={faPencilAlt}
              style={{ cursor: "pointer", marginLeft: "8px" }}
              onClick={phoneEditChangeHandler}
            />
          </div>
          {isPhoneEditing ? phoneEditingForm : null}
          <div
            style={{
              display: "flex",
              justifyContent: "flex-start",
              alignItems: "center"
            }}
          >
            <p>E-mail: {props.email}</p>
            <FontAwesomeIcon
              icon={faPencilAlt}
              style={{ cursor: "pointer", marginLeft: "8px" }}
              onClick={emailEditChangeHandler}
            />
          </div>
          {isEmailEditing ? emailEditingForm : null}
        </ProfileInfoContainer>
      </div>
    </React.Fragment>
  );
};

const mapStateToProps = state => {
  return {
    name: state.auth.name,
    accountNumber: state.auth.accountNumber,
    userId: state.auth.id,
    balance: state.account.balance,
    phoneNumber: state.auth.phoneNumber,
    email: state.auth.email,
    spotStatus: state.spot.status,
    spotAddress: state.spot.address,
    spotId: state.spot.spotId,
    token: state.auth.token
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onGetSpot: inputData => dispatch(actions.getSpot(inputData)),
    onGetAccount: inputData => dispatch(actions.getAccount(inputData)),
    onEditSpotStatus: inputData => dispatch(actions.editStatus(inputData)),
    onEditPhone: inputData => dispatch(actions.editPhone(inputData)),
    onEditEmail: inputData => dispatch(actions.editEmail(inputData))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withErrorHandler(Profile, axios));

const ProfileInfoContainer = styled.div`
  padding: 20px 10px;
`