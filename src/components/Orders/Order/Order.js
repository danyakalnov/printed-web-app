import React, { useEffect, useState } from 'react';
import classes from './Order.module.css';
import DocumentIcon from '../../UI/DocumentIcon/DocumentIcon';

import { connect } from 'react-redux';
import * as actions from '../../../store/actions/index';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCheckCircle } from '@fortawesome/free-solid-svg-icons';
import styled from 'styled-components';

import moment from 'moment';
import 'moment/locale/ru';

const BASEURL = 'https://printed-server.herokuapp.com';

moment.locale('ru');

const Order = props => {

  const [isOrderChecked, setOrderChecked] = useState(false);

  const checkOrderHandler = () => {
    setOrderChecked(true);

    const inputData = {
      spotId: props.spotId,
      orderId: props.orderId,
      newStatus: mapStatusesTransitions(props.status),
      token: props.token
    };

    props.onSetOrderStatus(inputData);
  };

  useEffect(() => {
    const inputData = {
      poolName: props.poolName,
      token: props.token,
      orderId: props.orderId,
    };
    props.onGetOrderDocuments(inputData);
  }, []);

  const mapReceiveOptions = option => {
    switch(option) {
      case 'university': return 'в университете';
      case 'personal': return 'лично';
      case 'dormitory': return 'в общежитии';
      default: 
        return 'в университете';
    }
  };

  const mapStatusesTransitions = oldStatus => {
    switch(oldStatus) {
      case 'placed': return 'inwork';
      case 'inwork': return 'ready';
      case 'ready': return 'received';
      default: 
        return 'received';
    }
  };

  const getCheckButtonText = orderStatus => {
    switch(orderStatus) {
      case 'placed': return 'Взять в работу';
      case 'inwork': return 'Выполнено';
      case 'ready': return 'Доставлено';
      default: 
        return 'Этот заказ уже готов!';
    }
  };

  const Button = styled.button`
    border-radius: 5px;
    border: none;
    background-color: #FBF8EF;
    padding: 10px 5px;
    font: inherit;
    cursor: pointer;
    outline: none;
    color: black;
    width: 25%;
    box-shadow: 0 2px 3px rgba(0, 0, 0, 0.26);
    &:hover {
      background-color: #EBE4CE;
      color: #353E4A;
    }
  `; 

  const InlineItemsDiv = styled.div`
    display: flex;
    flex-direction: row;
    margin: 0;
    width: 100%;
    align-items: center;
    justify-content: flex-start;
  `;

  const OrderPlacementTime = styled.p`
    color: ${props => props.isOld ? '#B16666' : '#66AEB1'}
  `;

  const placedTime = moment(props.createdAt);
  let daysDiff = new moment().diff(placedTime, 'days');
  let isOld = daysDiff > 21;

  let documentsRepresentation = null;
  let pagesCount = null;

  const allowDownloadDocuments = props.status !== 'placed';

  const order = props.orders.find(order => order.id === props.orderId);
  if (order.documents !== undefined) {
    const orderDocuments = order.documents.slice();
    const docsCount = orderDocuments.length;
    pagesCount = orderDocuments.reduce((count, doc) => count + doc.pagesCount, 0);
    pagesCount = <p>{docsCount > 1 ? 'Всего листов: ' : 'Листов: '}{pagesCount}</p>
    documentsRepresentation = orderDocuments.map(document => {
      if (allowDownloadDocuments) {
        return <a key={document.id} href={`${BASEURL}/documents/download/${document.id}`}><DocumentIcon /></a>
      }
      else {
        return <DocumentIcon key={document.id}/>
      }
  });
  }

  let orderCheckElement = (
    <Button
      disabled={props.status === 'received'}
      onClick={checkOrderHandler}>
      {getCheckButtonText(props.status)}
    </Button>
  );
  if (isOrderChecked || props.status === 'received') {
    orderCheckElement = <FontAwesomeIcon icon={faCheckCircle} size="2x" style={{
      color: '#8CC516'
    }} />
  }

  return (
    <div className={classes.Order}>
      <InlineItemsDiv>
        <p style={{color: '#66AEB1'}}>Размещён{'\u00A0'}</p>
        <OrderPlacementTime isOld={isOld}>
          <strong>{placedTime.format('LLL')}</strong>
        </OrderPlacementTime>
      </InlineItemsDiv>
      <InlineItemsDiv>
        <p>Документы: </p>
        {documentsRepresentation}
      </InlineItemsDiv>
      {pagesCount}
      <p>Способ получения: {mapReceiveOptions(props.receiveOption)}</p>
      <p>Стоимость заказа: <b>{props.cost} ₽</b></p>
      {orderCheckElement}
    </div>
  );
};

const mapStateToProps = (state, ownProps) => {
  return {
    token: state.auth.token,
    orders: state.order[ownProps.poolName],
    spotId: state.spot.spotId,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onGetOrderDocuments: inputData => dispatch(actions.getOrderDocuments(inputData)),
    onSetOrderStatus: inputData => dispatch(actions.setOrderStatus(inputData))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Order);