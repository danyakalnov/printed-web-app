import React, { useEffect, useState } from 'react';
import classes from './OrdersList.module.css';
import Order from '../Order/Order';

import { NavLink, withRouter } from 'react-router-dom';

import { connect } from 'react-redux';
import * as actions from '../../../store/actions/index';

import styled from 'styled-components';

const OrdersList = props => {

  const [sortType, setSortType] = useState('');

  useEffect(() => {
    const ordersStatus = props.poolName.slice(0, -6);
    console.log(ordersStatus);
    const inputData = {
      spotId: localStorage.getItem('spotId'),
      status: ordersStatus,
      token: props.token
    };
    console.log(inputData);
    props.onGetOrdersWithStatus(inputData);

    return () => {
      props.onCleanOrdersPool(props.poolName);
    }
  }, []);

  const SortOption = styled.p`
    font-weight: ${props => props.isActive ? 'bold' : 'normal'};
  `;

  const compareOrders = (sortType) => {
    switch (sortType) {
      case 'cost_asc': return (firstOrder, secondOrder) => firstOrder.cost - secondOrder.cost;
      case 'cost_desc': return (firstOrder, secondOrder) => secondOrder.cost - firstOrder.cost;
      case 'date_asc': return (firstOrder, secondOrder) => new Date(firstOrder.createdAt) - new Date(secondOrder.createdAt);
      case 'date_desc': return (firstOrder, secondOrder) => new Date(secondOrder.createdAt) - new Date(firstOrder.createdAt);
      default: 
        return () => {return};
    };
  };

  console.log(props.poolName);
  const fetchedOrders = props[`${props.poolName}`].slice();

  const ordersCount = fetchedOrders.length;

  let orders = <h4 style={{color: '#B16666', fontWeight: 'bold'}}>{props.noOrdersMessage}</h4>

  if (ordersCount !== 0) {
    orders = (
      <React.Fragment>
        <ul className={classes.Sorting}>
          <p style={{marginRight: '25px'}}>Сортировать по: </p>
          <li className={classes.SortOption}>
            <NavLink to={`${props.match.path}/?sort=cost_asc`} exact >
              <SortOption onClick={() => setSortType('cost_asc')} isActive={sortType === 'cost_asc'}>по возрастанию цены</SortOption>
            </NavLink>
          </li>
          <li className={classes.SortOption}>
            <NavLink to={`${props.match.path}/?sort=cost_desc`} exact >
              <SortOption onClick={() => setSortType('cost_desc')} isActive={sortType === 'cost_desc'}>по убыванию цены</SortOption>
            </NavLink>
          </li>
          <li className={classes.SortOption}>
            <NavLink to={`${props.match.path}/?sort=date_desc`} exact >
              <SortOption onClick={() => setSortType('date_desc')} isActive={sortType === 'date_desc'}>недавние</SortOption>
            </NavLink>
          </li>
          <li className={classes.SortOption}>
            <NavLink to={`${props.match.path}/?sort=date_asc`} exact >
              <SortOption onClick={() => setSortType('date_asc')} isActive={sortType === 'date_asc'}>старые</SortOption>
            </NavLink>
          </li>
        </ul>
        {fetchedOrders.sort(compareOrders(sortType)).map(order => {
          return (
            <Order 
              key={order.id}
              status={order.status}
              orderId={order.id}
              createdAt={order.createdAt}
              receiveOption={order.receiveOption}
              cost={order.cost}
              poolName={props.poolName}
            />
          );
        })}
      </React.Fragment>
    );
  }

  return(
    <div className={classes.OrdersList}>
      <h1>{props.poolTitle}</h1>
      {orders}
    </div>
  );
};

const mapStateToProps = (state, ownProps) => {
  return {
    token: state.auth.token,
    spotId: state.spot.spotId,
    [ownProps.poolName]: state.order[`${ownProps.poolName}`]
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onGetOrdersWithStatus: inputData => dispatch(actions.getOrdersWithStatus(inputData)),
    onCleanOrdersPool: poolName => dispatch(actions.cleanupOrdersPool(poolName))
  };
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(OrdersList));