import React, { useEffect, useState } from 'react';
import classes from '../OrdersList/OrdersList.module.css';
import Order from '../Order/Order';

import { NavLink, withRouter } from 'react-router-dom';

import { connect } from 'react-redux';
import * as actions from '../../../store/actions/index';

import styled from 'styled-components'

const OrdersPool = props => {

  const [sortType, setSortType] = useState('');

  useEffect(() => {
    props.onFetchOrdersPool(props.token);

    return () => {
      props.onCleanOrdersPool('ordersPool');
    }
  }, []);

  const SortOption = styled.p`
    font-weight: ${props => props.isActive ? 'bold' : 'normal'};
  `;

  const compareOrders = (sortType) => {
    switch (sortType) {
      case 'cost_asc': return (firstOrder, secondOrder) => firstOrder.cost - secondOrder.cost;
      case 'cost_desc': return (firstOrder, secondOrder) => secondOrder.cost - firstOrder.cost;
      case 'date_asc': return (firstOrder, secondOrder) => new Date(firstOrder.createdAt) - new Date(secondOrder.createdAt);
      case 'date_desc': return (firstOrder, secondOrder) => new Date(secondOrder.createdAt) - new Date(firstOrder.createdAt);
      default: 
        return () => {return};
    };
  };

  const fetchedOrders = props.ordersPool.slice();

  const ordersCount = fetchedOrders.length;

  let orders = <h4 style={{color: '#B16666', fontWeight: 'bold'}}>Пока нет доступных заказов :(</h4>

  if (ordersCount !== 0) {
    orders = (
      <React.Fragment>
        <ul className={classes.Sorting}>
          <p style={{marginRight: '25px'}}>Сортировать по: </p>
          <li className={classes.SortOption}>
            <NavLink to={`${props.match.path}/?sort=cost_asc`} exact >
              <SortOption onClick={() => setSortType('cost_asc')} isActive={sortType === 'cost_asc'}>по возрастанию цены</SortOption>
            </NavLink>
          </li>
          <li className={classes.SortOption}>
            <NavLink to={`${props.match.path}/?sort=cost_desc`} exact >
              <SortOption onClick={() => setSortType('cost_desc')} isActive={sortType === 'cost_desc'}>по убыванию цены</SortOption>
            </NavLink>
          </li>
          <li className={classes.SortOption}>
            <NavLink to={`${props.match.path}/?sort=date_desc`} exact >
              <SortOption onClick={() => setSortType('date_desc')} isActive={sortType === 'date_desc'}>недавние</SortOption>
            </NavLink>
          </li>
          <li className={classes.SortOption}>
            <NavLink to={`${props.match.path}/?sort=date_asc`} exact >
              <SortOption onClick={() => setSortType('date_asc')} isActive={sortType === 'date_asc'}>старые</SortOption>
            </NavLink>
          </li>
        </ul>
        {fetchedOrders.sort(compareOrders(sortType)).map(order => {
          return (
            <Order 
              key={order.id}
              status={order.status}
              orderId={order.id}
              createdAt={order.createdAt}
              receiveOption={order.receiveOption}
              cost={order.cost}
              poolName='ordersPool'
            />
          );
        })}
      </React.Fragment>
    );
  }

  return(
    <div className={classes.OrdersPool}>
      <h1>Пул заказов</h1>
      {orders}
    </div>
  );
};

const mapStateToProps = state => {
  return {
    token: state.auth.token,
    ordersPool: state.order.ordersPool
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onFetchOrdersPool: token => dispatch(actions.fetchOrdersPool(token)),
    onCleanOrdersPool: poolName => dispatch(actions.cleanupOrdersPool(poolName))
  };
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(OrdersPool));