import React from 'react';

const adapt = (Component, props) => ({
  input,
  meta: { valid },
  ...rest
}) => <Component {...input} {...rest} valid={valid} {...props}/>

export default adapt;