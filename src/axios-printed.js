import axios from 'axios';

const instance = axios.create({
  baseURL: 'https://printed-server.herokuapp.com'
});

export default instance;